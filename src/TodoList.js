import React from 'react';
import { Button, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from './store/actions';

// import styles from './styles';

const TodoList = ({ todos, addTodo }) => (
  <View>
    { todos.map(todo => <Text key={todo.id}>{todo.text}</Text>)}
    <Button title="Novo todo" onPress={addTodo} />
  </View>
);

const mapStateToProps = state => ({
  todos: state.todos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
