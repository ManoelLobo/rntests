import React from 'react';
import { Button, Text, View } from 'react-native';

// import styles from './styles';

const Post = ({ post, onDelete }) => (
  <View>
    <Text>{ post.title }</Text>
    <Text>{ post.description }</Text>
    <Button title="Remover post" onPress={() => {onDelete(post.id)}} />
  </View>
);

export default Post;
