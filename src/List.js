import React from 'react';
import { AsyncStorage, Button, ScrollView, Text, View } from 'react-native';

import Post from './Post';

// import styles from './styles';

class List extends React.Component {
  state = {
    posts: [],
  }

  async componentDidMount() {
    const posts = JSON.parse(await AsyncStorage.getItem('@teste:posts')) || [];

    this.setState({ posts })
  }

  renderPosts = () => (
    <ScrollView>
      { this.state.posts.map(post => <Post key={post.id} post={post} onDelete={this.deletePost} />) }
    </ScrollView>
  )

  addPost = () => {
    this.setState({
      posts: [
        ...this.state.posts,
        { id: Math.random(), title: 'Novo Post', description: 'Teste' },
      ],
    });
  }

  deletePost = (id) => {
    this.setState({
      posts: this.state.posts.filter(post => post.id !== id),
    });
  }

  savePosts = async () => {
    await AsyncStorage.setItem('@teste:posts', JSON.stringify(this.state.posts));
  }

  render() {
    return (
      <View>
        { this.state.posts.length > 0 ?
          this.renderPosts() :
          <Text>Nenhum post</Text>
        }
        <Button id="new" title="Novo post" onPress={this.addPost} />
        <Button id="save" title="Salvar posts" onPress={this.savePosts} />
      </View>
    );
  }
}

export default List;
